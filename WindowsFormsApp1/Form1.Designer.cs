﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.add_student_button = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.managment_signature_comboBoxAdd = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.student_signature_comboBoxAdd = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.diploma_RN_textBoxAdd = new System.Windows.Forms.TextBox();
            this.passport_textBoxAdd = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.stud_name_textBoxAdd = new System.Windows.Forms.TextBox();
            this.graduation_Year_dateTimePickerAdd = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.admission_Year_dateTimePickerAdd = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.diploma_status_comboBoxAdd = new System.Windows.Forms.ComboBox();
            this.diplomaForm_SN_textBoxAdd = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.traningDN_textBoxAdd = new System.Windows.Forms.TextBox();
            this.diploma_sup_form_SN_textBoxAdd = new System.Windows.Forms.TextBox();
            this.graduationExplusionOrder_Date_textBoxAdd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.stateCommissionProtocol_Date_textBoxAdd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.diploma_issue_dateTimePickerAdd = new System.Windows.Forms.DateTimePicker();
            this.honors_comboBoxAdd = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.assignedQualification_Name_textBoxAdd = new System.Windows.Forms.TextBox();
            this.traningDC_textBoxAdd = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.diploma_RN_textBoxS = new System.Windows.Forms.TextBox();
            this.update_button = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.Export_button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.passport_textBoxS = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.honors_comboBoxS = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.traningDN_textBoxS = new System.Windows.Forms.TextBox();
            this.assignedQualification_Name_textBoxS = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.stud_name_textBoxS = new System.Windows.Forms.TextBox();
            this.diploma_issue_dateTimePickerS = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.traningDC_textBoxS = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.WritingToTheDataBase_button = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 542);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.tableLayoutPanel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 516);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Добавить";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.add_student_button, 1, 17);
            this.tableLayoutPanel5.Controls.Add(this.label16, 0, 16);
            this.tableLayoutPanel5.Controls.Add(this.managment_signature_comboBoxAdd, 1, 16);
            this.tableLayoutPanel5.Controls.Add(this.label25, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this.student_signature_comboBoxAdd, 1, 15);
            this.tableLayoutPanel5.Controls.Add(this.label15, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this.diploma_RN_textBoxAdd, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.passport_textBoxAdd, 1, 14);
            this.tableLayoutPanel5.Controls.Add(this.label24, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this.stud_name_textBoxAdd, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.graduation_Year_dateTimePickerAdd, 1, 13);
            this.tableLayoutPanel5.Controls.Add(this.label14, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.admission_Year_dateTimePickerAdd, 1, 12);
            this.tableLayoutPanel5.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label23, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this.diploma_status_comboBoxAdd, 1, 11);
            this.tableLayoutPanel5.Controls.Add(this.diplomaForm_SN_textBoxAdd, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.label21, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label17, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.traningDN_textBoxAdd, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.diploma_sup_form_SN_textBoxAdd, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.graduationExplusionOrder_Date_textBoxAdd, 1, 10);
            this.tableLayoutPanel5.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.stateCommissionProtocol_Date_textBoxAdd, 1, 9);
            this.tableLayoutPanel5.Controls.Add(this.label13, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.diploma_issue_dateTimePickerAdd, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.honors_comboBoxAdd, 1, 8);
            this.tableLayoutPanel5.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.assignedQualification_Name_textBoxAdd, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.traningDC_textBoxAdd, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 18;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(786, 510);
            this.tableLayoutPanel5.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Регистрационный номер диплома";
            // 
            // add_student_button
            // 
            this.add_student_button.Location = new System.Drawing.Point(285, 449);
            this.add_student_button.Name = "add_student_button";
            this.add_student_button.Size = new System.Drawing.Size(94, 37);
            this.add_student_button.TabIndex = 18;
            this.add_student_button.Text = "Добавить";
            this.add_student_button.UseVisualStyleBackColor = true;
            this.add_student_button.Click += new System.EventHandler(this.add_student_button_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 419);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Подпись руководства";
            // 
            // managment_signature_comboBoxAdd
            // 
            this.managment_signature_comboBoxAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.managment_signature_comboBoxAdd.FormattingEnabled = true;
            this.managment_signature_comboBoxAdd.Items.AddRange(new object[] {
            "Имеется",
            "Остутсвует"});
            this.managment_signature_comboBoxAdd.Location = new System.Drawing.Point(285, 422);
            this.managment_signature_comboBoxAdd.Name = "managment_signature_comboBoxAdd";
            this.managment_signature_comboBoxAdd.Size = new System.Drawing.Size(162, 21);
            this.managment_signature_comboBoxAdd.TabIndex = 17;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 340);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Год выпуска";
            // 
            // student_signature_comboBoxAdd
            // 
            this.student_signature_comboBoxAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.student_signature_comboBoxAdd.FormattingEnabled = true;
            this.student_signature_comboBoxAdd.Items.AddRange(new object[] {
            "Имеется",
            "Остутсвует"});
            this.student_signature_comboBoxAdd.Location = new System.Drawing.Point(285, 395);
            this.student_signature_comboBoxAdd.Name = "student_signature_comboBoxAdd";
            this.student_signature_comboBoxAdd.Size = new System.Drawing.Size(162, 21);
            this.student_signature_comboBoxAdd.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 392);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(218, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Подпись лица, которому выдан документ";
            // 
            // diploma_RN_textBoxAdd
            // 
            this.diploma_RN_textBoxAdd.Location = new System.Drawing.Point(285, 3);
            this.diploma_RN_textBoxAdd.Name = "diploma_RN_textBoxAdd";
            this.diploma_RN_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.diploma_RN_textBoxAdd.TabIndex = 1;
            // 
            // passport_textBoxAdd
            // 
            this.passport_textBoxAdd.Location = new System.Drawing.Point(285, 369);
            this.passport_textBoxAdd.Name = "passport_textBoxAdd";
            this.passport_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.passport_textBoxAdd.TabIndex = 15;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 314);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Год поступления";
            // 
            // stud_name_textBoxAdd
            // 
            this.stud_name_textBoxAdd.Location = new System.Drawing.Point(285, 29);
            this.stud_name_textBoxAdd.Name = "stud_name_textBoxAdd";
            this.stud_name_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.stud_name_textBoxAdd.TabIndex = 2;
            // 
            // graduation_Year_dateTimePickerAdd
            // 
            this.graduation_Year_dateTimePickerAdd.Location = new System.Drawing.Point(285, 343);
            this.graduation_Year_dateTimePickerAdd.Name = "graduation_Year_dateTimePickerAdd";
            this.graduation_Year_dateTimePickerAdd.Size = new System.Drawing.Size(162, 20);
            this.graduation_Year_dateTimePickerAdd.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 366);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Паспортные данные";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "ФИО";
            // 
            // admission_Year_dateTimePickerAdd
            // 
            this.admission_Year_dateTimePickerAdd.Location = new System.Drawing.Point(285, 317);
            this.admission_Year_dateTimePickerAdd.Name = "admission_Year_dateTimePickerAdd";
            this.admission_Year_dateTimePickerAdd.Size = new System.Drawing.Size(162, 20);
            this.admission_Year_dateTimePickerAdd.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Серия и номер бланка диплома";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 287);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Статус диплома";
            // 
            // diploma_status_comboBoxAdd
            // 
            this.diploma_status_comboBoxAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.diploma_status_comboBoxAdd.FormattingEnabled = true;
            this.diploma_status_comboBoxAdd.Items.AddRange(new object[] {
            "Выдан",
            "Не выдан",
            "Утерян (выдан дублика)"});
            this.diploma_status_comboBoxAdd.Location = new System.Drawing.Point(285, 290);
            this.diploma_status_comboBoxAdd.Name = "diploma_status_comboBoxAdd";
            this.diploma_status_comboBoxAdd.Size = new System.Drawing.Size(162, 21);
            this.diploma_status_comboBoxAdd.TabIndex = 12;
            // 
            // diplomaForm_SN_textBoxAdd
            // 
            this.diplomaForm_SN_textBoxAdd.Location = new System.Drawing.Point(285, 55);
            this.diplomaForm_SN_textBoxAdd.Name = "diplomaForm_SN_textBoxAdd";
            this.diplomaForm_SN_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.diplomaForm_SN_textBoxAdd.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 78);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(202, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Серия и номер приложения к диплому";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 156);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(213, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Наименование направления подготовки";
            // 
            // traningDN_textBoxAdd
            // 
            this.traningDN_textBoxAdd.Location = new System.Drawing.Point(285, 159);
            this.traningDN_textBoxAdd.Name = "traningDN_textBoxAdd";
            this.traningDN_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.traningDN_textBoxAdd.TabIndex = 7;
            // 
            // diploma_sup_form_SN_textBoxAdd
            // 
            this.diploma_sup_form_SN_textBoxAdd.Location = new System.Drawing.Point(285, 81);
            this.diploma_sup_form_SN_textBoxAdd.Name = "diploma_sup_form_SN_textBoxAdd";
            this.diploma_sup_form_SN_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.diploma_sup_form_SN_textBoxAdd.TabIndex = 4;
            // 
            // graduationExplusionOrder_Date_textBoxAdd
            // 
            this.graduationExplusionOrder_Date_textBoxAdd.Location = new System.Drawing.Point(285, 264);
            this.graduationExplusionOrder_Date_textBoxAdd.Name = "graduationExplusionOrder_Date_textBoxAdd";
            this.graduationExplusionOrder_Date_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.graduationExplusionOrder_Date_textBoxAdd.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Дата выдачи диплома";
            // 
            // stateCommissionProtocol_Date_textBoxAdd
            // 
            this.stateCommissionProtocol_Date_textBoxAdd.Location = new System.Drawing.Point(285, 238);
            this.stateCommissionProtocol_Date_textBoxAdd.Name = "stateCommissionProtocol_Date_textBoxAdd";
            this.stateCommissionProtocol_Date_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.stateCommissionProtocol_Date_textBoxAdd.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 261);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(262, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Дата и номер приказа об отчислении выпускника";
            // 
            // diploma_issue_dateTimePickerAdd
            // 
            this.diploma_issue_dateTimePickerAdd.Location = new System.Drawing.Point(285, 107);
            this.diploma_issue_dateTimePickerAdd.Name = "diploma_issue_dateTimePickerAdd";
            this.diploma_issue_dateTimePickerAdd.Size = new System.Drawing.Size(162, 20);
            this.diploma_issue_dateTimePickerAdd.TabIndex = 5;
            // 
            // honors_comboBoxAdd
            // 
            this.honors_comboBoxAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.honors_comboBoxAdd.FormattingEnabled = true;
            this.honors_comboBoxAdd.Items.AddRange(new object[] {
            "Да",
            "Нет"});
            this.honors_comboBoxAdd.Location = new System.Drawing.Point(285, 211);
            this.honors_comboBoxAdd.Name = "honors_comboBoxAdd";
            this.honors_comboBoxAdd.Size = new System.Drawing.Size(162, 21);
            this.honors_comboBoxAdd.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Код направления подготовки";
            // 
            // assignedQualification_Name_textBoxAdd
            // 
            this.assignedQualification_Name_textBoxAdd.Location = new System.Drawing.Point(285, 185);
            this.assignedQualification_Name_textBoxAdd.Name = "assignedQualification_Name_textBoxAdd";
            this.assignedQualification_Name_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.assignedQualification_Name_textBoxAdd.TabIndex = 8;
            // 
            // traningDC_textBoxAdd
            // 
            this.traningDC_textBoxAdd.Location = new System.Drawing.Point(285, 133);
            this.traningDC_textBoxAdd.Name = "traningDC_textBoxAdd";
            this.traningDC_textBoxAdd.Size = new System.Drawing.Size(162, 20);
            this.traningDC_textBoxAdd.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 235);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(276, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Дата и номер протокола государственной комиссии";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(235, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Наименование присвоенной квалификацией";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Диплом с отличием";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel3);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(792, 516);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Поиск";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.dataGridView2, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(792, 516);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.diploma_RN_textBoxS, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.update_button, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Export_button, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.passport_textBoxS, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.honors_comboBoxS, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.traningDN_textBoxS, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.assignedQualification_Name_textBoxS, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.label20, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.stud_name_textBoxS, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.diploma_issue_dateTimePickerS, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.traningDC_textBoxS, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.label18, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(324, 510);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // diploma_RN_textBoxS
            // 
            this.diploma_RN_textBoxS.Location = new System.Drawing.Point(165, 3);
            this.diploma_RN_textBoxS.Name = "diploma_RN_textBoxS";
            this.diploma_RN_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.diploma_RN_textBoxS.TabIndex = 1;
            this.diploma_RN_textBoxS.TextChanged += new System.EventHandler(this.diploma_RN_textBoxS_TextChanged);
            // 
            // update_button
            // 
            this.update_button.Location = new System.Drawing.Point(3, 212);
            this.update_button.Name = "update_button";
            this.update_button.Size = new System.Drawing.Size(75, 23);
            this.update_button.TabIndex = 10;
            this.update_button.Text = "Обновить";
            this.update_button.UseVisualStyleBackColor = true;
            this.update_button.Click += new System.EventHandler(this.update_button_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Рег номер диплома";
            // 
            // Export_button
            // 
            this.Export_button.Location = new System.Drawing.Point(165, 212);
            this.Export_button.Name = "Export_button";
            this.Export_button.Size = new System.Drawing.Size(75, 23);
            this.Export_button.TabIndex = 9;
            this.Export_button.Text = "Экспорт";
            this.Export_button.UseVisualStyleBackColor = true;
            this.Export_button.Click += new System.EventHandler(this.Export_button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Паспортные данные";
            // 
            // passport_textBoxS
            // 
            this.passport_textBoxS.Location = new System.Drawing.Point(165, 186);
            this.passport_textBoxS.Name = "passport_textBoxS";
            this.passport_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.passport_textBoxS.TabIndex = 8;
            this.passport_textBoxS.TextChanged += new System.EventHandler(this.passport_textBoxS_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Направление подготовки";
            // 
            // honors_comboBoxS
            // 
            this.honors_comboBoxS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.honors_comboBoxS.FormattingEnabled = true;
            this.honors_comboBoxS.Items.AddRange(new object[] {
            "Да",
            "Нет"});
            this.honors_comboBoxS.Location = new System.Drawing.Point(165, 159);
            this.honors_comboBoxS.Name = "honors_comboBoxS";
            this.honors_comboBoxS.Size = new System.Drawing.Size(153, 21);
            this.honors_comboBoxS.TabIndex = 7;
            this.honors_comboBoxS.SelectionChangeCommitted += new System.EventHandler(this.honors_comboBoxS_SelectionChangeCommitted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Диплом с отличием";
            // 
            // traningDN_textBoxS
            // 
            this.traningDN_textBoxS.Location = new System.Drawing.Point(165, 107);
            this.traningDN_textBoxS.Name = "traningDN_textBoxS";
            this.traningDN_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.traningDN_textBoxS.TabIndex = 5;
            this.traningDN_textBoxS.TextChanged += new System.EventHandler(this.traningDN_textBoxS_TextChanged);
            // 
            // assignedQualification_Name_textBoxS
            // 
            this.assignedQualification_Name_textBoxS.Location = new System.Drawing.Point(165, 133);
            this.assignedQualification_Name_textBoxS.Name = "assignedQualification_Name_textBoxS";
            this.assignedQualification_Name_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.assignedQualification_Name_textBoxS.TabIndex = 6;
            this.assignedQualification_Name_textBoxS.TextChanged += new System.EventHandler(this.assignedQualification_Name_textBoxS_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "ФИО";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Квалификация";
            // 
            // stud_name_textBoxS
            // 
            this.stud_name_textBoxS.Location = new System.Drawing.Point(165, 29);
            this.stud_name_textBoxS.Name = "stud_name_textBoxS";
            this.stud_name_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.stud_name_textBoxS.TabIndex = 2;
            this.stud_name_textBoxS.TextChanged += new System.EventHandler(this.stud_name_textBoxS_TextChanged);
            // 
            // diploma_issue_dateTimePickerS
            // 
            this.diploma_issue_dateTimePickerS.Location = new System.Drawing.Point(165, 55);
            this.diploma_issue_dateTimePickerS.Name = "diploma_issue_dateTimePickerS";
            this.diploma_issue_dateTimePickerS.Size = new System.Drawing.Size(153, 20);
            this.diploma_issue_dateTimePickerS.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Дата выдачи диплома";
            // 
            // traningDC_textBoxS
            // 
            this.traningDC_textBoxS.Location = new System.Drawing.Point(165, 81);
            this.traningDC_textBoxS.Name = "traningDC_textBoxS";
            this.traningDC_textBoxS.Size = new System.Drawing.Size(153, 20);
            this.traningDC_textBoxS.TabIndex = 4;
            this.traningDC_textBoxS.TextChanged += new System.EventHandler(this.traningDC_textBoxS_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(156, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Код направления подготовки";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(333, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView2.Size = new System.Drawing.Size(456, 510);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.TabStop = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(792, 516);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Список студентов";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.button3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.dataGridView4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(792, 516);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 467);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 32);
            this.button3.TabIndex = 1;
            this.button3.Text = "Обновление";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(786, 458);
            this.dataGridView4.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 516);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "SQL запрос";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox4, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(786, 510);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(780, 402);
            this.dataGridView1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 462);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 36);
            this.button2.TabIndex = 0;
            this.button2.Text = "Поиск";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(3, 411);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(778, 29);
            this.textBox4.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.WritingToTheDataBase_button);
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Controls.Add(this.toolStrip1);
            this.tabPage3.Controls.Add(this.menuStrip1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(792, 516);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Excel";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // WritingToTheDataBase_button
            // 
            this.WritingToTheDataBase_button.Location = new System.Drawing.Point(175, 24);
            this.WritingToTheDataBase_button.Name = "WritingToTheDataBase_button";
            this.WritingToTheDataBase_button.Size = new System.Drawing.Size(103, 25);
            this.WritingToTheDataBase_button.TabIndex = 3;
            this.WritingToTheDataBase_button.Text = "Записать в базу данных";
            this.WritingToTheDataBase_button.UseVisualStyleBackColor = true;
            this.WritingToTheDataBase_button.Click += new System.EventHandler(this.WritingToTheDataBase_button_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 49);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(792, 467);
            this.dataGridView3.TabIndex = 2;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripComboBox1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(792, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(33, 22);
            this.toolStripLabel1.Text = "Лист";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(792, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.файлToolStripMenuItem.Text = "Файл...";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel|*.xlsx";
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(792, 516);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "Создать файл";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(629, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Создать файл";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 542);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox diplomaForm_SN_textBoxAdd;
        private System.Windows.Forms.TextBox stud_name_textBoxAdd;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button add_student_button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox diploma_RN_textBoxAdd;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DateTimePicker diploma_issue_dateTimePickerAdd;
        private System.Windows.Forms.Button Export_button;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TextBox diploma_sup_form_SN_textBoxAdd;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox honors_comboBoxAdd;
        private System.Windows.Forms.TextBox assignedQualification_Name_textBoxAdd;
        private System.Windows.Forms.TextBox traningDC_textBoxAdd;
        private System.Windows.Forms.ComboBox managment_signature_comboBoxAdd;
        private System.Windows.Forms.ComboBox student_signature_comboBoxAdd;
        private System.Windows.Forms.TextBox passport_textBoxAdd;
        private System.Windows.Forms.TextBox graduationExplusionOrder_Date_textBoxAdd;
        private System.Windows.Forms.TextBox stateCommissionProtocol_Date_textBoxAdd;
        private System.Windows.Forms.TextBox traningDN_textBoxAdd;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox traningDN_textBoxS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox passport_textBoxS;
        private System.Windows.Forms.ComboBox honors_comboBoxS;
        private System.Windows.Forms.TextBox assignedQualification_Name_textBoxS;
        private System.Windows.Forms.TextBox traningDC_textBoxS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker diploma_issue_dateTimePickerS;
        private System.Windows.Forms.TextBox diploma_RN_textBoxS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox stud_name_textBoxS;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox diploma_status_comboBoxAdd;
        private System.Windows.Forms.DateTimePicker graduation_Year_dateTimePickerAdd;
        private System.Windows.Forms.DateTimePicker admission_Year_dateTimePickerAdd;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button WritingToTheDataBase_button;
        private System.Windows.Forms.Button update_button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button1;
    }
}

